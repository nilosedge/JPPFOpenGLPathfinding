package org.jppf.application.template;

import java.awt.Point;

public interface FractalFunctionInterface {

	public Point3D computePoint(Point.Double p);
}
