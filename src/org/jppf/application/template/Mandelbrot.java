package org.jppf.application.template;

import java.awt.Point;

public class Mandelbrot extends FractalFunction {
	
	/*
	 * Z = Z^2 + C
	 * (x + yi) = ((X^2 + 2xyi - Y^2) + (x + yi)
	 * 
	 * x = X^2 - Y^2 + x
	 * y = 2xyi + yi
	 * 
	 */
	
	public Mandelbrot(FractalFunctionParameters params) {
		super(params);
	}
	
	public Point3D computeFractal(Point.Double p) {

		double xc = 0;
		double yc = 0;
		double xt = 0;

		int iteration = 0;
		boolean lessThen = false;
		double xcs = 0;
		double ycs = 0;

		//System.out.println("X: " + p.x + " Y: " + p.y);

		do {
			xcs = xc * xc;
			ycs = yc * yc;
			xt = (xcs - ycs) + p.x;
			yc = (yc * xc * 2) + p.y;
			xc = xt;
			lessThen = (xcs + ycs) <= params.infinity;
		} while(lessThen && ++iteration <= params.maxIter);

		return new Point3D(xc, yc, iteration-1);
	}
}