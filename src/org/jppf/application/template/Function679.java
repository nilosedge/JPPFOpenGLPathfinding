package org.jppf.application.template;

import java.awt.Point;

public class Function679 extends FractalFunction {
	
	/*
	 * (x + yi) = (Z^Z / (1 - Z)) + Z + C
	 * 
	 * (x + yi) = ( (X^2 + 2xyi - Y^2) / (1 - (x + yi))) + (x + yi) + (a + bi)
	 * 
	 * x = (X^2 - Y^2 / (1 - x)) + x + a
	 * y = (2xy / (1 - y)) + y + b
	 * 
	 */
	
	public Function679(FractalFunctionParameters params) {
		super(params);
	}
	
	public Point3D computeFractal(Point.Double p) {

		double xc = params.p1;
		double yc = params.p2;
		double xt = 0;

		int iteration = 0;
		boolean lessThen = false;
		double xc2 = 0;
		double yc2 = 0;
		
		do {
			
			xc2 = xc * xc;
			yc2 = yc * yc;

			xt = ((xc2 - yc2) / (1 - xc)) + xc + p.x;
			yc = ((2 * xc * yc) / (1 - yc)) + yc + p.y;
			xc = xt;
			lessThen = (xc2 + yc2) <= params.infinity;
		} while(lessThen && iteration++ < params.maxIter);

		return new Point3D(xc, yc, iteration);
	}
}