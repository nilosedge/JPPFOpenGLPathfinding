package org.jppf.application.template;

import java.awt.Color;
import java.util.HashMap;

public class Colorator {

	public enum Palette {
		HUE,
		RAINBOW
		;
	}
	
	private Palette pal;
	private HashMap<Palette, Color[]> colorMap = new HashMap<Palette, Color[]>();
	//private int colorOffset = 0;
	private int colorMax = 256;
	
	public Colorator(Palette pal) {
		this.pal = pal;
		initColors();
	}
	
	public Color getColor(double zeroToOne) {
		if(zeroToOne < 0) zeroToOne = 0;
		if(zeroToOne > 1) zeroToOne = 1;
		Color[] colors = colorMap.get(pal);
		return colors[(int)(zeroToOne * (colors.length - 1))];
	}
	
	private void initColors() {
		/*
		000000 Black
		FF0000 Red
		FFFF00 Yellow
		0FFFF0 Green
		00FFFF Cyan
		0000FF Blue
		FF00FF Magenta
		000000 Black
		*/
		
		Color[] colors = new Color[colorMax*7];
		for(int i = 0; i < colorMax; i++) {
			colors[i + (colorMax * 0)] = new Color(i, 0, 0);
			colors[i + (colorMax * 1)] = new Color((colorMax-1), i, 0);
			colors[i + (colorMax * 2)] = new Color((colorMax-1)-i, (colorMax-1), 0);
			colors[i + (colorMax * 3)] = new Color(0, (colorMax-1), i);
			colors[i + (colorMax * 4)] = new Color(0, (colorMax-1)-i, (colorMax-1));
			colors[i + (colorMax * 5)] = new Color(i, 0, (colorMax-1));
			colors[i + (colorMax * 6)] = new Color((colorMax-1)-i, 0, (colorMax-1)-i);
		}
		colorMap.put(Palette.RAINBOW, colors);
		
		int max = 1000;
		colors = new Color[max];
        for (int i = 0; i<max; i++) {
            colors[i] = new Color(Color.HSBtoRGB(i/256f, 1, i/(i+8f)));
        }
        colorMap.put(Palette.HUE, colors);
	}

//	public void incrementColors() {
//		colorOffset += (int)(colorMap.get(pal).length * 0.01);
//	}
	
}
