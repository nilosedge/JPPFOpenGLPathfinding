package org.jppf.application.template;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FractalConfig implements Serializable {

	public String id;
	public int w;
	public int h;
	public double sx;
	public double sy;
	public double tx;
	public double ty;
	public double iter;
	
}
