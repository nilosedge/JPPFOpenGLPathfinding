/*
 * JPPF.
 * Copyright (C) 2005-2015 JPPF Team.
 * http://www.jppf.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jppf.application.template;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import org.jppf.node.protocol.AbstractTask;

@SuppressWarnings("serial")
public class TemplateJPPFTask extends AbstractTask<String> {

	private FractalConfig config;

	public TemplateJPPFTask(FractalConfig config) {
		this.config = config;
	}

	@Override
	public void run() {
		System.out.println("Starting Image generation");
		BufferedImage image = GenFractal.generateFractal(config);
		try {
			System.out.println("Saving File: /export/" + config.id + ".png");
			ImageIO.write(image, "png", new FileOutputStream(new File("/export/" + config.id + ".png")));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finished Image generation");
		setResult("Success: /export/" + config.id + ".png");
	}
}
