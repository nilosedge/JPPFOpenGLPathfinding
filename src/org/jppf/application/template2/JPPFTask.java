package org.jppf.application.template2;


import org.jppf.node.protocol.AbstractTask;

@SuppressWarnings("serial")
public class JPPFTask extends AbstractTask<String> {

	private String name;
	private long sleep;
	
	public JPPFTask(String name, long sleep) {
		this.name = name;
		this.sleep = sleep;
	}

	@Override
	public void run() {
		System.out.println(name + ": Started");
		System.out.println(name + " is running for " + (sleep * 10000) + " cycles");

		for(int i = 0; i < sleep * 10000; i++) {
			int j = (int)Math.sqrt(Math.random() * 50000);
		}
		System.out.println(name + ": Finished Successful");
		
		setResult(name + ": Successful");
	}
}
