package org.jppf.application.template2;

import java.util.Date;

public class Timer {

	private Date start;
	
	public void start() {
		start = new Date();
	}
	
	public void time() {
		Date end = new Date();
		System.out.println("Duraction: " + (end.getTime() - start.getTime()) + "ms");
	}

}
