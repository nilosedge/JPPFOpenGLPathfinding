package org.jppf.application.template2;

import java.util.List;

import org.jppf.client.JPPFClient;
import org.jppf.client.JPPFJob;
import org.jppf.node.protocol.Task;

public class JPPFMain2 {

	public static void main(String[] args) {
		Timer t = new Timer();
		t.start();
		try (JPPFClient jppfClient = new JPPFClient()) {

			JPPFJob job = new JPPFJob();
			job.setName("Job Name");
			
			for(int i = 0; i < 50; i++) {
				job.add(new JPPFTask("Task " + i, 400));
			}
			
			jppfClient.submitJob(job);
			
			List<Task<?>> results = job.awaitResults();

			for (Task<?> task: results) {
				String taskId = task.getId();
				
				if (task.getThrowable() == null) {
					System.out.println("Execution result: " + task.getResult());
				} else {
					System.out.println(taskId + ", an exception was raised: " + task.getThrowable().getMessage());
				}
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
		t.time();
	}
}
